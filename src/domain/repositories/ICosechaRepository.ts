import { Cosecha } from "../../infrastructure/entities/Cosecha";

export interface ICosechaRepository {
    findAll(): Promise<Cosecha[]>;
    findById(id: number): Promise<Cosecha | null>;
    create(cosecha: Cosecha): Promise<Cosecha>;
    update(id: number, cosecha: Cosecha): Promise<Cosecha | null>;
    delete(id: number): Promise<boolean>;
    
}
