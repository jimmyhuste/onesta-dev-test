import { Campo } from "../../infrastructure/entities/Campo";

export interface ICampoRepository {
    findAll(): Promise<Campo[]>;
    findById(id: number): Promise<Campo | null>;
    create(campo: Campo): Promise<Campo>;
    update(id: number, campo: Campo): Promise<Campo | null>;
    delete(id: number): Promise<boolean>;
}
