import { Cliente } from "../../infrastructure/entities/Cliente";

export interface IClienteRepository {
    findAll(): Promise<Cliente[]>;
    findById(id: number): Promise<Cliente | null>;
    create(cliente: Cliente): Promise<Cliente>;
    update(id: number, cliente: Cliente): Promise<Cliente | null>;
    delete(id: number): Promise<boolean>;
}
