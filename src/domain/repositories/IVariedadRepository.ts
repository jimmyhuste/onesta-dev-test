import { Variedad } from '../../infrastructure/entities/Variedad';

export interface IVariedadRepository {
    findAll(): Promise<Variedad[]>;
    findById(id: number): Promise<Variedad | null>;
    create(variedad: Variedad): Promise<Variedad>;
    update(id: number, variedad: Variedad): Promise<Variedad | null>;
    delete(id: number): Promise<boolean>;
}
