import { Fruta } from '../../infrastructure/entities/Fruta';

export interface IFrutaRepository {
    findAll(): Promise<Fruta[]>;
    findById(id: number): Promise<Fruta | null>;
    create(fruta: Fruta): Promise<Fruta>;
    update(id: number, fruta: Fruta): Promise<Fruta | null>;
    delete(id: number): Promise<boolean>;
}
