import { Agricultor } from "../../infrastructure/entities/Agricultor";

export interface IAgricultorRepository {
    findAll(): Promise<Agricultor[]>;
    findById(id: number): Promise<Agricultor | null>;
    create(agricultor: Agricultor): Promise<Agricultor>;
    update(id: number, agricultor: Agricultor): Promise<Agricultor | null>;
    delete(id: number): Promise<boolean>;
}
