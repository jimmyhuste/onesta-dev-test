import { FrutaService } from '../../../application/services/FrutaService';
import { Fruta } from '../../../infrastructure/entities/Fruta';

describe('Tests para FrutaService', () => {
  let frutaService: FrutaService;

  beforeEach(() => {
    frutaService = new FrutaService({
      findAll: jest.fn(),
      findById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    });
  });

  it('Debería obtener todas las frutas', async () => {
    const mockFrutas: Fruta[] = [{ id: 1, nombre: 'Manzana' }];
    (frutaService.frutaRepository.findAll as jest.Mock).mockResolvedValue(mockFrutas);

    const frutas = await frutaService.getAll();
    expect(frutas).toEqual(mockFrutas);
  });

  it('Debería obtener una fruta por su ID', async () => {
    const mockFruta: Fruta = { id: 1, nombre: 'Manzana' };
    (frutaService.frutaRepository.findById as jest.Mock).mockResolvedValue(mockFruta);

    const fruta = await frutaService.getById(1);
    expect(fruta).toEqual(mockFruta);
  });

  it('Debería crear una nueva fruta', async () => {
    const mockFruta: Fruta = { id: 1, nombre: 'Manzana' };
    (frutaService.frutaRepository.create as jest.Mock).mockResolvedValue(mockFruta);

    const newFruta = await frutaService.create(mockFruta);
    expect(newFruta).toEqual(mockFruta);
  });

  it('Debería actualizar una fruta existente', async () => {
    const mockFruta: Fruta = { id: 1, nombre: 'Manzana' };
    (frutaService.frutaRepository.update as jest.Mock).mockResolvedValue(mockFruta);

    const updatedFruta = await frutaService.update(1, mockFruta);
    expect(updatedFruta).toEqual(mockFruta);
  });

  it('Debería eliminar una fruta existente', async () => {
    const mockSuccess = true;
    (frutaService.frutaRepository.delete as jest.Mock).mockResolvedValue(mockSuccess);

    const success = await frutaService.delete(1);
    expect(success).toEqual(mockSuccess);
  });


});
