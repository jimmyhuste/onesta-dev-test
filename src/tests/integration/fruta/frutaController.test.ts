import { Request, Response } from 'express';
import { FrutaService } from '../../../application/services/FrutaService';
import { FrutaController } from '../../../interfaces/controllers/frutaController';

describe('Tests para FrutaController', () => {
  let frutaController: FrutaController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockFrutaService: jest.Mocked<FrutaService>;


  beforeEach(() => {
    mockFrutaService = {
      getAll: jest.fn(),
      getById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<FrutaService>;

    mockFrutaService = {} as jest.Mocked<FrutaService>;
    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('Debería obtener todas las frutas', async () => {
    const mockFrutas = [{ id: 1, nombre: 'Manzana' }];
    (mockFrutaService.getAll as jest.Mock).mockResolvedValue(mockFrutas);

    await frutaController.getAll(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockFrutas);
  });

  it('Debería obtener una fruta por su ID', async () => {
    const mockFruta = { id: 1, nombre: 'Manzana' };
    (mockFrutaService.getById as jest.Mock).mockResolvedValue(mockFruta);

    mockRequest.params = { id: '1' };
    await frutaController.getById(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockFruta);
  });

  it('Debería crear una nueva fruta', async () => {
    const mockFruta = { id: 1, nombre: 'Manzana' };
    (mockFrutaService.create as jest.Mock).mockResolvedValue(mockFruta);

    mockRequest.body = mockFruta;
    await frutaController.create(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockFruta);
  });

  it('Debería actualizar una fruta existente', async () => {
    const mockFruta = { id: 1, nombre: 'Manzana' };
    (mockFrutaService.update as jest.Mock).mockResolvedValue(mockFruta);

    mockRequest.params = { id: '1' };
    mockRequest.body = mockFruta;
    await frutaController.update(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockFruta);
  });

  it('Debería eliminar una fruta existente', async () => {
    const mockSuccess = true;
    (mockFrutaService.delete as jest.Mock).mockResolvedValue(mockSuccess);

    mockRequest.params = { id: '1' };
    await frutaController.delete(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(204);
  });

});
