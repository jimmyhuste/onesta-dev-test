import { Request, Response } from 'express';
import { ClienteService } from '../../../application/services/clienteService';
import { ClienteController } from '../../../interfaces/controllers/clienteController';

describe('Tests para ClienteController', () => {
  let clienteController: ClienteController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockClienteService: jest.Mocked<ClienteService>;

  beforeEach(() => {
    mockClienteService = {
      getAll: jest.fn(),
      getById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<ClienteService>;

    clienteController = new ClienteController(mockClienteService);

    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('Debería obtener todos los clientes', async () => {
    const mockClientes = [{ id: 1, nombre: 'Cliente 1' }];
    (mockClienteService.getAll as jest.Mock).mockResolvedValue(mockClientes);

    await clienteController.getAll(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockClientes);
  });

  it('Debería obtener un cliente por su ID', async () => {
    const mockCliente = { id: 1, nombre: 'Cliente 1' };
    (mockClienteService.getById as jest.Mock).mockResolvedValue(mockCliente);

    mockRequest.params = { id: '1' };
    await clienteController.getById(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockCliente);
  });

  it('Debería crear un nuevo cliente', async () => {
    const mockCliente = { id: 1, nombre: 'Cliente 1' };
    (mockClienteService.create as jest.Mock).mockResolvedValue(mockCliente);

    mockRequest.body = mockCliente;
    await clienteController.create(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockCliente);
  });

  it('Debería actualizar un cliente existente', async () => {
    const mockCliente = { id: 1, nombre: 'Cliente 1' };
    (mockClienteService.update as jest.Mock).mockResolvedValue(mockCliente);

    mockRequest.params = { id: '1' };
    mockRequest.body = mockCliente;
    await clienteController.update(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockCliente);
  });

  it('Debería eliminar un cliente existente', async () => {
    const mockSuccess = true;
    (mockClienteService.delete as jest.Mock).mockResolvedValue(mockSuccess);

    mockRequest.params = { id: '1' };
    await clienteController.delete(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(204);
  });

});
