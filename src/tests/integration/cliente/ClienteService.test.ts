import { ClienteService } from '../../../application/services/clienteService';
import { Cliente } from '../../../infrastructure/entities/Cliente';

describe('Tests para ClienteService', () => {
  let clienteService: ClienteService;

  beforeEach(() => {
    clienteService = new ClienteService({
      findAll: jest.fn(),
      findById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    });
  });

  it('Debería obtener todos los clientes', async () => {
    const mockClientes: Cliente[] = [{ id: 1, nombre: 'Cliente 1', email: 'cliente1@example.com' }];
    (clienteService.clienteRepository.findAll as jest.Mock).mockResolvedValue(mockClientes);

    const clientes = await clienteService.getAll();
    expect(clientes).toEqual(mockClientes);
  });

  it('Debería obtener un cliente por su ID', async () => {
    const mockCliente: Cliente = { id: 1, nombre: 'Cliente 1', email: 'cliente1@example.com' };
    (clienteService.clienteRepository.findById as jest.Mock).mockResolvedValue(mockCliente);

    const cliente = await clienteService.getById(1);
    expect(cliente).toEqual(mockCliente);
  });

  it('Debería crear un nuevo cliente', async () => {
    const mockCliente: Cliente = { id: 1, nombre: 'Cliente 1', email: 'cliente1@example.com' };
    (clienteService.clienteRepository.create as jest.Mock).mockResolvedValue(mockCliente);

    const newCliente = await clienteService.create(mockCliente);
    expect(newCliente).toEqual(mockCliente);
  });

  it('Debería actualizar un cliente existente', async () => {
    const mockCliente: Cliente = { id: 1, nombre: 'Cliente 1', email: 'cliente1@example.com' };
    (clienteService.clienteRepository.update as jest.Mock).mockResolvedValue(mockCliente);

    const updatedCliente = await clienteService.update(1, mockCliente);
    expect(updatedCliente).toEqual(mockCliente);
  });

  it('Debería eliminar un cliente existente', async () => {
    const mockSuccess = true;
    (clienteService.clienteRepository.delete as jest.Mock).mockResolvedValue(mockSuccess);

    const success = await clienteService.delete(1);
    expect(success).toEqual(mockSuccess);
  });

});
