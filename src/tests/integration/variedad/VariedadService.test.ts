import { VariedadService } from '../../../application/services/variedadService';
import { Variedad } from '../../../infrastructure/entities/Variedad';
import { VariedadRepository } from '../../../infrastructure/repositories/variedadRepository';

describe('Tests para VariedadService', () => {
  let variedadService: VariedadService;
  let mockVariedadRepository: jest.Mocked<VariedadRepository>;

  beforeEach(() => {
    mockVariedadRepository = {
      findAll: jest.fn(),
      findById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<VariedadRepository>;

    variedadService = new VariedadService(mockVariedadRepository);
  });

  it('Debería obtener todas las variedades', async () => {
    const mockVariedades: Variedad[] = [{ id: 1, nombre: 'Variedad 1', fruta: { id: 1, nombre: 'Fruta 1' } }];
    mockVariedadRepository.findAll.mockResolvedValue(mockVariedades);

    const variedades = await variedadService.getAll();

    expect(variedades).toEqual(mockVariedades);
  });

  it('Debería obtener una variedad por su ID', async () => {
    const mockVariedad: Variedad = { id: 1, nombre: 'Variedad 1', fruta: { id: 1, nombre: 'Fruta 1' } };
    mockVariedadRepository.findById.mockResolvedValue(mockVariedad);

    const variedad = await variedadService.getById(1);

    expect(variedad).toEqual(mockVariedad);
  });

  it('Debería crear una nueva variedad', async () => {
    const mockVariedad: Variedad = { id: 1, nombre: 'Variedad 1', fruta: { id: 1, nombre: 'Fruta 1' } };
    mockVariedadRepository.create.mockResolvedValue(mockVariedad);

    const newVariedad = await variedadService.create(mockVariedad);

    expect(newVariedad).toEqual(mockVariedad);
  });

  it('Debería actualizar una variedad existente', async () => {
    const mockVariedad: Variedad = { id: 1, nombre: 'Variedad 1', fruta: { id: 1, nombre: 'Fruta 1' } };
    mockVariedadRepository.update.mockResolvedValue(mockVariedad);

    const updatedVariedad = await variedadService.update(1, mockVariedad);

    expect(updatedVariedad).toEqual(mockVariedad);
  });

  it('Debería eliminar una variedad existente', async () => {
    const mockSuccess = true;
    mockVariedadRepository.delete.mockResolvedValue(mockSuccess);

    const success = await variedadService.delete(1);

    expect(success).toEqual(mockSuccess);
  });
});
