import { Request, Response } from 'express';
import { VariedadService } from '../../../application/services/variedadService';
import { VariedadController } from '../../../interfaces/controllers/variedadController';

describe('VariedadController', () => {
  let variedadController: VariedadController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockVariedadService: jest.Mocked<VariedadService>;

  beforeEach(() => {
    mockVariedadService = {
      getAll: jest.fn(),
      getById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<VariedadService>;

    variedadController = new VariedadController(mockVariedadService);

    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('should get all varieties', async () => {
    const mockVariedades = [{ id: 1, nombre: 'Variedad 1' }];
    (mockVariedadService.getAll as jest.Mock).mockResolvedValue(mockVariedades);

    await variedadController.getAll(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockVariedades);
  });

  it('should get a variety by its ID', async () => {
    const mockVariedad = { id: 1, nombre: 'Variedad 1' };
    (mockVariedadService.getById as jest.Mock).mockResolvedValue(mockVariedad);

    mockRequest.params = { id: '1' };
    await variedadController.getById(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockVariedad);
  });

  it('should create a new variety', async () => {
    const mockVariedad = { id: 1, nombre: 'Variedad 1' };
    (mockVariedadService.create as jest.Mock).mockResolvedValue(mockVariedad);

    mockRequest.body = mockVariedad;
    await variedadController.create(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockVariedad);
  });

  it('should update an existing variety', async () => {
    const mockVariedad = { id: 1, nombre: 'Variedad 1' };
    (mockVariedadService.update as jest.Mock).mockResolvedValue(mockVariedad);

    mockRequest.params = { id: '1' };
    mockRequest.body = mockVariedad;
    await variedadController.update(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockVariedad);
  });

  it('should delete an existing variety', async () => {
    const mockSuccess = true;
    (mockVariedadService.delete as jest.Mock).mockResolvedValue(mockSuccess);

    mockRequest.params = { id: '1' };
    await variedadController.delete(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(204);
  });
});
