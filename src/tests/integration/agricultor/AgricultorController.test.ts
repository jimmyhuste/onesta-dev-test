import { Request, Response } from 'express';
import { AgricultorService } from '../../../application/services/agricultorService';
import { AgricultorController } from '../../../interfaces/controllers/agricultorController';

describe('Tests para AgricultorController', () => {
  let agricultorController: AgricultorController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockAgricultorService: jest.Mocked<AgricultorService>;

  beforeEach(() => {
    mockAgricultorService = {
      getAll: jest.fn(),
      getById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<AgricultorService>;

    agricultorController = new AgricultorController();

    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('Debería obtener todos los agricultores', async () => {
    const mockAgricultores = [{ id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' }];
    (mockAgricultorService.getAll as jest.Mock).mockResolvedValue(mockAgricultores);

    await agricultorController.getAll(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockAgricultores);
  });

  it('Debería obtener un agricultor por su ID', async () => {
    const mockAgricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorService.getById as jest.Mock).mockResolvedValue(mockAgricultor);

    mockRequest.params = { id: '1' };
    await agricultorController.getById(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockAgricultor);
  });

  it('Debería crear un nuevo agricultor', async () => {
    const mockAgricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorService.create as jest.Mock).mockResolvedValue(mockAgricultor);

    mockRequest.body = mockAgricultor;
    await agricultorController.create(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockAgricultor);
  });

  it('Debería actualizar un agricultor existente', async () => {
    const mockAgricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorService.update as jest.Mock).mockResolvedValue(mockAgricultor);

    mockRequest.params = { id: '1' };
    mockRequest.body = mockAgricultor;
    await agricultorController.update(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockAgricultor);
  });

  it('Debería eliminar un agricultor existente', async () => {
    const mockSuccess = true;
    (mockAgricultorService.delete as jest.Mock).mockResolvedValue(mockSuccess);

    mockRequest.params = { id: '1' };
    await agricultorController.delete(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(204);
  });

});
