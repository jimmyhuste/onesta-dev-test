import { AgricultorService } from '../../../application/services/agricultorService';
import { Agricultor } from '../../../infrastructure/entities/Agricultor';
import { AgricultorRepository } from '../../../infrastructure/repositories/agricultorRepository';

describe('Tests para AgricultorService', () => {
  let agricultorService: AgricultorService;
  let mockAgricultorRepository: jest.Mocked<AgricultorRepository>;

  beforeEach(() => {
    mockAgricultorRepository = {
      findAll: jest.fn(),
      findById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<AgricultorRepository>;

    agricultorService = new AgricultorService(mockAgricultorRepository);
  });

  it('Debería obtener todos los agricultores', async () => {
    const mockAgricultores: Agricultor[] = [{ id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' }];
    (mockAgricultorRepository.findAll as jest.Mock).mockResolvedValue(mockAgricultores);

    const agricultores = await agricultorService.getAll();
    expect(agricultores).toEqual(mockAgricultores);
  });

  it('Debería obtener un agricultor por su ID', async () => {
    const mockAgricultor: Agricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorRepository.findById as jest.Mock).mockResolvedValue(mockAgricultor);

    const agricultor = await agricultorService.getById(1);
    expect(agricultor).toEqual(mockAgricultor);
  });

  it('Debería crear un nuevo agricultor', async () => {
    const mockAgricultor: Agricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorRepository.create as jest.Mock).mockResolvedValue(mockAgricultor);

    const newAgricultor = await agricultorService.create(mockAgricultor);
    expect(newAgricultor).toEqual(mockAgricultor);
  });

  it('Debería actualizar un agricultor existente', async () => {
    const mockAgricultor: Agricultor = { id: 1, nombre: 'Agricultor 1', email: 'agricultor1@example.com' };
    (mockAgricultorRepository.update as jest.Mock).mockResolvedValue(mockAgricultor);

    const updatedAgricultor = await agricultorService.update(1, mockAgricultor);
    expect(updatedAgricultor).toEqual(mockAgricultor);
  });

  it('Debería eliminar un agricultor existente', async () => {
    const mockSuccess = true;
    (mockAgricultorRepository.delete as jest.Mock).mockResolvedValue(mockSuccess);

    const success = await agricultorService.delete(1);
    expect(success).toEqual(mockSuccess);
  });

});
