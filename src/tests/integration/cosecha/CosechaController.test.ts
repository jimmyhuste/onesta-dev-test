import { Request, Response } from 'express';
import { CosechaService } from '../../../application/services/cosechaService';
import { CosechaController } from '../../../interfaces/controllers/cosechaController';

describe('Tests para CosechaController', () => {
  let cosechaController: CosechaController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockCosechaService: jest.Mocked<CosechaService>;

  beforeEach(() => {
    mockCosechaService = {
      getAll: jest.fn(),
      getById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<CosechaService>;

    cosechaController = new CosechaController(mockCosechaService);

    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('Debería obtener todas las cosechas', async () => {
    const mockCosechas = [{ id: 1, fecha: new Date(), cantidad: 10 }];
    (mockCosechaService.getAll as jest.Mock).mockResolvedValue(mockCosechas);

    await cosechaController.getAll(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockCosechas);
  });

  it('Debería obtener una cosecha por su ID', async () => {
    const mockCosecha = { id: 1, fecha: new Date(), cantidad: 10 };
    (mockCosechaService.getById as jest.Mock).mockResolvedValue(mockCosecha);

    mockRequest.params = { id: '1' };
    await cosechaController.getById(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockCosecha);
  });

  it('Debería crear una nueva cosecha', async () => {
    const mockCosecha = { id: 1, fecha: new Date(), cantidad: 10 };
    (mockCosechaService.create as jest.Mock).mockResolvedValue(mockCosecha);

    mockRequest.body = mockCosecha;
    await cosechaController.create(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockCosecha);
  });

  it('Debería actualizar una cosecha existente', async () => {
    const mockCosecha = { id: 1, fecha: new Date(), cantidad: 10 };
    (mockCosechaService.update as jest.Mock).mockResolvedValue(mockCosecha);

    mockRequest.params = { id: '1' };
    mockRequest.body = mockCosecha;
    await cosechaController.update(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.json).toHaveBeenCalledWith(mockCosecha);
  });

  it('Debería eliminar una cosecha existente', async () => {
    const mockSuccess = true;
    (mockCosechaService.delete as jest.Mock).mockResolvedValue(mockSuccess);

    mockRequest.params = { id: '1' };
    await cosechaController.delete(mockRequest as Request, mockResponse as Response);

    expect(mockResponse.status).toHaveBeenCalledWith(204);
  });

});