import { CosechaService } from '../../../application/services/cosechaService';
import { ICosechaRepository } from '../../../domain/repositories/ICosechaRepository';
import { Agricultor } from '../../../infrastructure/entities/Agricultor';
import { Cosecha } from '../../../infrastructure/entities/Cosecha';

describe('Tests para CosechaService', () => {
  let cosechaService: CosechaService;
  let mockCosechaRepository: jest.Mocked<ICosechaRepository>;

  beforeEach(() => {
    mockCosechaRepository = {
      findAll: jest.fn(),
      findById: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    };

    cosechaService = new CosechaService(mockCosechaRepository);
  });

  const mockAgricultor: Agricultor = {
    id: 1,
    nombre: 'Agricultor 1',
    cosechas: [],
    email: 'jimmy@gmail.com'
  };

  it('Debería obtener todas las cosechas', async () => {
    const mockCosechas: Cosecha[] = [
      { id: 1, fecha: new Date(), cantidad: 10, agricultor: mockAgricultor }
    ];
    mockCosechaRepository.findAll.mockResolvedValue(mockCosechas);

    const cosechas = await cosechaService.getAll();
    expect(cosechas).toEqual(mockCosechas);
  });

  it('Debería obtener una cosecha por su ID', async () => {
    const mockCosecha: Cosecha = { id: 1, fecha: new Date(), cantidad: 10, agricultor: mockAgricultor };
    mockCosechaRepository.findById.mockResolvedValue(mockCosecha);

    const cosecha = await cosechaService.getById(1);
    expect(cosecha).toEqual(mockCosecha);
  });

  it('Debería crear una nueva cosecha', async () => {
    const mockCosecha: Cosecha = { id: 1, fecha: new Date(), cantidad: 10, agricultor: mockAgricultor };
    mockCosechaRepository.create.mockResolvedValue(mockCosecha);

    const newCosecha = await cosechaService.create(mockCosecha);
    expect(newCosecha).toEqual(mockCosecha);
  });

  it('Debería actualizar una cosecha existente', async () => {
    const mockCosecha: Cosecha = { id: 1, fecha: new Date(), cantidad: 10, agricultor: mockAgricultor };
    mockCosechaRepository.update.mockResolvedValue(mockCosecha);

    const updatedCosecha = await cosechaService.update(1, mockCosecha);
    expect(updatedCosecha).toEqual(mockCosecha);
  });

  it('Debería eliminar una cosecha existente', async () => {
    const mockSuccess = true;
    mockCosechaRepository.delete.mockResolvedValue(mockSuccess);

    const success = await cosechaService.delete(1);
    expect(success).toEqual(mockSuccess);
  });
});
