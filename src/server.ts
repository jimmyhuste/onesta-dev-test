import bodyParser from 'body-parser';
import express from 'express';
import 'reflect-metadata';
import { AppDataSource } from './infrastructure/database/AppDataSource';
import agricultorRoutes from './interfaces/routes/agricultorRoutes';
import campoRoutes from './interfaces/routes/campoRoutes';
import clienteRoutes from './interfaces/routes/clienteRoutes';
import cosechaRoutes from './interfaces/routes/cosechaRoutes';
import frutaRoutes from './interfaces/routes/frutaRoutes';
import variedadRoutes from './interfaces/routes/variedadRoutes';

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware para permitir CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*'); // Permitir solicitudes de cualquier origen
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); // Permitir los métodos HTTP especificados
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept'); // Permitir los encabezados especificados
    next();
});

app.use(bodyParser.json());

AppDataSource.initialize().then(() => {
    console.log('Database connected');
    app.use('/frutas', frutaRoutes);
    app.use('/variedades', variedadRoutes);
    app.use('/cosechas', cosechaRoutes);
    app.use('/clientes', clienteRoutes);
    app.use('/agricultores', agricultorRoutes);
    app.use('/campos', campoRoutes);

    app.listen(PORT, () => {
        console.log(`Server running on port ${PORT}`);
    });
}).catch((error) => console.log(error));
