import { ICampoRepository } from '../../domain/repositories/ICampoRepository';
import { Campo } from '../../infrastructure/entities/Campo';

export class CampoService {
    constructor(public campoRepository: ICampoRepository) {}

    async getAll(): Promise<Campo[]> {
        return this.campoRepository.findAll();
    }

    async getById(id: number): Promise<Campo | null> {
        return this.campoRepository.findById(id);
    }

    async create(campo: Campo): Promise<Campo> {
        return this.campoRepository.create(campo);
    }

    async update(id: number, campo: Campo): Promise<Campo | null> {
        return this.campoRepository.update(id, campo);
    }

    async delete(id: number): Promise<boolean> {
        return this.campoRepository.delete(id);
    }
}
