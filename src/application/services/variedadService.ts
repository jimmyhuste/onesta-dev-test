import { IVariedadRepository } from '../../domain/repositories/IVariedadRepository';
import { Variedad } from '../../infrastructure/entities/Variedad';

export class VariedadService {
    constructor(public variedadRepository: IVariedadRepository) {}

    async getAll(): Promise<Variedad[]> {
        return this.variedadRepository.findAll();
    }

    async getById(id: number): Promise<Variedad | null> {
        return this.variedadRepository.findById(id);
    }

    async create(variedad: Variedad): Promise<Variedad> {
        return this.variedadRepository.create(variedad);
    }

    async update(id: number, variedad: Variedad): Promise<Variedad | null> {
        return this.variedadRepository.update(id, variedad);
    }

    async delete(id: number): Promise<boolean> {
        return this.variedadRepository.delete(id);
    }
}
