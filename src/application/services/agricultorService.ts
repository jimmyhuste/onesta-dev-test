import { IAgricultorRepository } from '../../domain/repositories/IAgricultorRepository';
import { Agricultor } from '../../infrastructure/entities/Agricultor';

export class AgricultorService {
    constructor(public agricultorRepository: IAgricultorRepository) {}

    async getAll(): Promise<Agricultor[]> {
        return this.agricultorRepository.findAll();
    }

    async getById(id: number): Promise<Agricultor | null> {
        return this.agricultorRepository.findById(id);
    }

    async create(agricultor: Agricultor): Promise<Agricultor> {
        return this.agricultorRepository.create(agricultor);
    }

    async update(id: number, agricultor: Agricultor): Promise<Agricultor | null> {
        return this.agricultorRepository.update(id, agricultor);
    }

    async delete(id: number): Promise<boolean> {
        return this.agricultorRepository.delete(id);
    }
}
