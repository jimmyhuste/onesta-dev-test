import { IClienteRepository } from '../../domain/repositories/IClienteRepository';
import { Cliente } from '../../infrastructure/entities/Cliente';

export class ClienteService {
    constructor(public clienteRepository: IClienteRepository) {}

    async getAll(): Promise<Cliente[]> {
        return this.clienteRepository.findAll();
    }

    async getById(id: number): Promise<Cliente | null> {
        return this.clienteRepository.findById(id);
    }

    async create(cliente: Cliente): Promise<Cliente> {
        return this.clienteRepository.create(cliente);
    }

    async update(id: number, cliente: Cliente): Promise<Cliente | null> {
        return this.clienteRepository.update(id, cliente);
    }

    async delete(id: number): Promise<boolean> {
        return this.clienteRepository.delete(id);
    }
}
