import { IFrutaRepository } from '../../domain/repositories/IFrutaRepository';
import { Fruta } from '../../infrastructure/entities/Fruta';

export class FrutaService {
    constructor(public frutaRepository: IFrutaRepository) {}

    async getAll(): Promise<Fruta[]> {
        return this.frutaRepository.findAll();
    }

    async getById(id: number): Promise<Fruta | null> {
        return this.frutaRepository.findById(id);
    }

    async create(fruta: Fruta): Promise<Fruta> {
        return this.frutaRepository.create(fruta);
    }

    async update(id: number, fruta: Fruta): Promise<Fruta | null> {
        return this.frutaRepository.update(id, fruta);
    }

    async delete(id: number): Promise<boolean> {
        return this.frutaRepository.delete(id);
    }
}
