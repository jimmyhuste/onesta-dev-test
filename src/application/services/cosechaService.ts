import { ICosechaRepository } from '../../domain/repositories/ICosechaRepository';
import { Cosecha } from '../../infrastructure/entities/Cosecha';

export class CosechaService {
    constructor(public cosechaRepository: ICosechaRepository) {}

    async getAll(): Promise<Cosecha[]> {
        return this.cosechaRepository.findAll();
    }

    async getById(id: number): Promise<Cosecha | null> {
        return this.cosechaRepository.findById(id);
    }

    async create(cosecha: Cosecha): Promise<Cosecha> {
        return this.cosechaRepository.create(cosecha);
    }

    async update(id: number, cosecha: Cosecha): Promise<Cosecha | null> {
        return this.cosechaRepository.update(id, cosecha);
    }

    async delete(id: number): Promise<boolean> {
        return this.cosechaRepository.delete(id);
    }
}
