import { DataSource } from 'typeorm';
import { Agricultor } from '../entities/Agricultor';
import { Campo } from '../entities/Campo';
import { Cliente } from '../entities/Cliente';
import { Cosecha } from '../entities/Cosecha';
import { Fruta } from '../entities/Fruta';
import { Variedad } from '../entities/Variedad';

// Inicialización diferida para garantizar que se ejecute solo una vez
class AppDataSourceInitializer {
    private static instance: DataSource | null = null;

    public static initialize(): DataSource {
        if (!AppDataSourceInitializer.instance) {
            AppDataSourceInitializer.instance = new DataSource({
                type: 'sqlite',
                database: './fruitapi.db',
                entities: [Agricultor, Campo, Cliente, Cosecha, Fruta, Variedad],
                synchronize: true,
            });
        }
        return AppDataSourceInitializer.instance;
    }
}

export const AppDataSource = AppDataSourceInitializer.initialize();
