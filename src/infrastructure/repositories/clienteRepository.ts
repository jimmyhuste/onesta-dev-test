import { Repository } from 'typeorm';
import { IClienteRepository } from '../../domain/repositories/IClienteRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Cliente } from '../entities/Cliente';

export class ClienteRepository implements IClienteRepository {
    private repository: Repository<Cliente>;

    constructor() {
        this.repository = AppDataSource.getRepository(Cliente);
    }

    async findAll(): Promise<Cliente[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Cliente | null> {
        return this.repository.findOneBy({ id });
    }

    async create(cliente: Cliente): Promise<Cliente> {
        return this.repository.save(cliente);
    }

    async update(id: number, cliente: Cliente): Promise<Cliente | null> {
        const existingCliente = await this.repository.findOneBy({ id });
        if (!existingCliente) {
            return null;
        }
        Object.assign(existingCliente, cliente);
        return this.repository.save(existingCliente);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
