import { Repository } from 'typeorm';
import { ICampoRepository } from '../../domain/repositories/ICampoRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Campo } from '../entities/Campo';

export class CampoRepository implements ICampoRepository {
    private repository: Repository<Campo>;

    constructor() {
        this.repository = AppDataSource.getRepository(Campo);
    }

    async findAll(): Promise<Campo[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Campo | null> {
        return this.repository.findOneBy({ id });
    }

    async create(campo: Campo): Promise<Campo> {
        return this.repository.save(campo);
    }

    async update(id: number, campo: Campo): Promise<Campo | null> {
        const existingCampo = await this.repository.findOneBy({ id });
        if (!existingCampo) {
            return null;
        }
        Object.assign(existingCampo, campo);
        return this.repository.save(existingCampo);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
