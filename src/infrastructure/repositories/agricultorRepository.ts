import { Repository } from 'typeorm';
import { IAgricultorRepository } from '../../domain/repositories/IAgricultorRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Agricultor } from '../entities/Agricultor';

export class AgricultorRepository implements IAgricultorRepository {
    private repository: Repository<Agricultor>;

    constructor() {
        this.repository = AppDataSource.getRepository(Agricultor);
    }

    async findAll(): Promise<Agricultor[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Agricultor | null> {
        return this.repository.findOneBy({ id });
    }

    async create(agricultor: Agricultor): Promise<Agricultor> {
        return this.repository.save(agricultor);
    }

    async update(id: number, agricultor: Agricultor): Promise<Agricultor | null> {
        const existingAgricultor = await this.repository.findOneBy({ id });
        if (!existingAgricultor) {
            return null;
        }
        Object.assign(existingAgricultor, agricultor);
        return this.repository.save(existingAgricultor);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
