import { Repository } from 'typeorm';
import { IFrutaRepository } from '../../domain/repositories/IFrutaRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Fruta } from '../entities/Fruta';

export class FrutaRepository implements IFrutaRepository {
    private repository: Repository<Fruta>;

    constructor() {
        this.repository = AppDataSource.getRepository(Fruta);
    }

    async findAll(): Promise<Fruta[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Fruta | null> {
        return this.repository.findOneBy({ id });
    }

    async create(fruta: Fruta): Promise<Fruta> {
        return this.repository.save(fruta);
    }

    async update(id: number, fruta: Fruta): Promise<Fruta | null> {
        const existingFruta = await this.repository.findOneBy({ id });
        if (!existingFruta) {
            return null;
        }
        Object.assign(existingFruta, fruta);
        return this.repository.save(existingFruta);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
