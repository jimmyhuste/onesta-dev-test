import { Repository } from 'typeorm';
import { IVariedadRepository } from '../../domain/repositories/IVariedadRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Variedad } from '../entities/Variedad';

export class VariedadRepository implements IVariedadRepository {
    private repository: Repository<Variedad>;

    constructor() {
        this.repository = AppDataSource.getRepository(Variedad);
    }

    async findAll(): Promise<Variedad[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Variedad | null> {
        return this.repository.findOneBy({ id });
    }

    async create(variedad: Variedad): Promise<Variedad> {
        return this.repository.save(variedad);
    }

    async update(id: number, variedad: Variedad): Promise<Variedad | null> {
        const existingVariedad = await this.repository.findOneBy({ id });
        if (!existingVariedad) {
            return null;
        }
        Object.assign(existingVariedad, variedad);
        return this.repository.save(existingVariedad);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
