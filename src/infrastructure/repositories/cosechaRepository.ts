import { Repository } from 'typeorm';
import { ICosechaRepository } from '../../domain/repositories/ICosechaRepository';
import { AppDataSource } from '../database/AppDataSource';
import { Cosecha } from '../entities/Cosecha';

export class CosechaRepository implements ICosechaRepository {
    private repository: Repository<Cosecha>;

    constructor() {
        this.repository = AppDataSource.getRepository(Cosecha);
    }

    async findAll(): Promise<Cosecha[]> {
        return this.repository.find();
    }

    async findById(id: number): Promise<Cosecha | null> {
        return this.repository.findOneBy({ id });
    }

    async create(cosecha: Cosecha): Promise<Cosecha> {
        return this.repository.save(cosecha);
    }

    async update(id: number, cosecha: Cosecha): Promise<Cosecha | null> {
        const existingCosecha = await this.repository.findOneBy({ id });
        if (!existingCosecha) {
            return null;
        }
        Object.assign(existingCosecha, cosecha);
        return this.repository.save(existingCosecha);
    }

    async delete(id: number): Promise<boolean> {
        const result = await this.repository.delete(id);
        return result.affected !== 0;
    }
}
