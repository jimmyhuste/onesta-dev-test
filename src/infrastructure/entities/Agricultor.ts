import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Campo } from './Campo';
import { Cosecha } from './Cosecha';

@Entity()
@Unique(['email'])
export class Agricultor {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    nombre!: string;

    @Column()
    email!: string;

    @OneToMany(() => Cosecha, (cosecha: Cosecha) => cosecha.agricultor)
    cosechas?: Cosecha[];

    @OneToMany(() => Campo, (campo: Campo) => campo.agricultor)
    campos?: Campo[];
}
