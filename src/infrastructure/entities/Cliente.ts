import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Unique(['email'])
export class Cliente {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    nombre!: string;

    @Column()
    email!: string;
}
