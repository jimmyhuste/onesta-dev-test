import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Fruta } from './Fruta';

@Entity()
@Unique(['nombre', 'fruta'])
export class Variedad {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    nombre!: string;

    @ManyToOne(() => Fruta, (fruta: Fruta) => fruta.variedades)
    fruta!: Fruta;
}
