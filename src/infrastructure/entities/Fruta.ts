import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Variedad } from './Variedad';

@Entity()
export class Fruta {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ unique: true })
    nombre!: string;

    @OneToMany(() => Variedad, (variedad: Variedad) => variedad.fruta)
    variedades?: Variedad[];
}
