import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Agricultor } from './Agricultor';

@Entity()
export class Cosecha {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    fecha!: Date;

    @Column()
    cantidad!: number;

    @ManyToOne(() => Agricultor, (agricultor: Agricultor) => agricultor.cosechas)
    agricultor!: Agricultor;
}
