import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Agricultor } from './Agricultor';

@Entity()
@Unique(['nombre', 'ubicacion'])
export class Campo {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    nombre!: string;

    @Column()
    ubicacion!: string;

    @ManyToOne(() => Agricultor, (agricultor: Agricultor) => agricultor.campos)
    agricultor!: Agricultor;
}
