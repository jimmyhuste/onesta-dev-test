## Endpoints de la API

El host de la API es `http://localhost:3000`.

### Módulo Fruta:
- `GET` `/frutas`: Obtener todas las frutas.
- `GET` `/frutas/:id`: Obtener una fruta por su ID.
- `POST` `/frutas`: Crear una nueva fruta.
- `PUT` `/frutas/:id`: Actualizar una fruta existente por su ID.
- `DELETE` `/frutas/:id`: Eliminar una fruta por su ID.

### Módulo Variedad:
- `GET` `/variedades`: Obtener todas las variedades.
- `GET` `/variedades/:id`: Obtener una variedad por su ID.
- `POST` `/variedades`: Crear una nueva variedad.
- `PUT` `/variedades/:id`: Actualizar una variedad existente por su ID.
- `DELETE` `/variedades/:id`: Eliminar una variedad por su ID.

### Módulo Cosecha:
- `GET` `/cosechas`: Obtener todas las cosechas.
- `GET` `/cosechas/:id`: Obtener una cosecha por su ID.
- `POST` `/cosechas`: Crear una nueva cosecha.
- `PUT` `/cosechas/:id`: Actualizar una cosecha existente por su ID.
- `DELETE` `/cosechas/:id`: Eliminar una cosecha por su ID.

### Módulo Cliente:
- `GET` `/clientes`: Obtener todos los clientes.
- `GET` `/clientes/:id`: Obtener un cliente por su ID.
- `POST` `/clientes`: Crear un nuevo cliente.
- `PUT` `/clientes/:id`: Actualizar un cliente existente por su ID.
- `DELETE` `/clientes/:id`: Eliminar un cliente por su ID.

### Módulo Agricultor:
- `GET` `/agricultores`: Obtener todos los agricultores.
- `GET` `/agricultores/:id`: Obtener un agricultor por su ID.
- `POST` `/agricultores`: Crear un nuevo agricultor.
- `PUT` `/agricultores/:id`: Actualizar un agricultor existente por su ID.
- `DELETE` `/agricultores/:id`: Eliminar un agricultor por su ID.

### Módulo Campo:
- `GET` `/campos`: Obtener todos los campos.
- `GET` `/campos/:id`: Obtener un campo por su ID.
- `POST` `/campos`: Crear un nuevo campo.
- `PUT` `/campos/:id`: Actualizar un campo existente por su ID.
- `DELETE` `/campos/:id`: Eliminar un campo por su ID.

#  Curls

la collection se encuentra en la carpeta curl en el siguiente archivo: 
```typescript-test.postman_collection.json```