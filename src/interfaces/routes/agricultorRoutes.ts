import express from 'express';
import { AgricultorController } from '../controllers/agricultorController';

const router = express.Router();
const controller = new AgricultorController();

router.get('/', controller.getAll);
router.get('/:id', controller.getById);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

export default router;
