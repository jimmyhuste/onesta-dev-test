import express from 'express';
import { ClienteService } from '../../application/services/clienteService';
import { ClienteRepository } from '../../infrastructure/repositories/clienteRepository';
import { ClienteController } from '../controllers/clienteController';

const router = express.Router();
const clienteRepository = new ClienteRepository(); 
const clienteService = new ClienteService(clienteRepository); 
const controller = new ClienteController(clienteService); 

router.get('/', (req, res) => controller.getAll(req, res));
router.get('/:id', (req, res) => controller.getById(req, res));
router.post('/', (req, res) => controller.create(req, res));
router.put('/:id', (req, res) => controller.update(req, res));
router.delete('/:id', (req, res) => controller.delete(req, res));

export default router;
