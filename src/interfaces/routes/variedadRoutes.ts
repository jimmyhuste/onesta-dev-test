import { Router } from 'express';
import { VariedadService } from '../../application/services/variedadService';
import { VariedadRepository } from '../../infrastructure/repositories/variedadRepository';
import { VariedadController } from '../controllers/variedadController';

const router = Router();

// Inicializar VariedadService y VariedadController
const variedadRepository = new VariedadRepository();
const variedadService = new VariedadService(variedadRepository);
const controller = new VariedadController(variedadService);

// Definir rutas
router.get('/', (req, res) => controller.getAll(req, res));
router.get('/:id', (req, res) => controller.getById(req, res));
router.post('/', (req, res) => controller.create(req, res));
router.put('/:id', (req, res) => controller.update(req, res));
router.delete('/:id', (req, res) => controller.delete(req, res));

export default router;
