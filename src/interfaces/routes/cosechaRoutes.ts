import { Router } from 'express';
import { CosechaService } from '../../application/services/cosechaService';
import { CosechaController } from '../controllers/cosechaController';

import { CosechaRepository } from '../../infrastructure/repositories/cosechaRepository';

const cosechaRepository = new CosechaRepository(); 

const cosechaService = new CosechaService(cosechaRepository);

const controller = new CosechaController(cosechaService);

const router = Router();

router.get('/', (req, res) => controller.getAll(req, res));
router.get('/:id', (req, res) => controller.getById(req, res));
router.post('/', (req, res) => controller.create(req, res));
router.put('/:id', (req, res) => controller.update(req, res));
router.delete('/:id', (req, res) => controller.delete(req, res));

export default router;
