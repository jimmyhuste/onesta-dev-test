import { Request, Response } from 'express';
import { VariedadService } from '../../application/services/variedadService';
import { VariedadRepository } from '../../infrastructure/repositories/variedadRepository';

const variedadRepository = new VariedadRepository();
const variedadService = new VariedadService(variedadRepository);

export class VariedadController {
    constructor(private variedadService: VariedadService) {}

    async getAll(req: Request, res: Response): Promise<Response> {
        const variedades = await this.variedadService.getAll();
        return res.json(variedades);
    }

    async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const variedad = await this.variedadService.getById(Number(id));
        if (!variedad) {
            return res.status(404).json({ message: 'Variedad not found' });
        }
        return res.json(variedad);
    }

    async create(req: Request, res: Response): Promise<Response> {
        const variedad = req.body;
        const newVariedad = await this.variedadService.create(variedad);
        return res.status(201).json(newVariedad);
    }

    async update(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const variedad = req.body;
        const updatedVariedad = await this.variedadService.update(Number(id), variedad);
        if (!updatedVariedad) {
            return res.status(404).json({ message: 'Variedad not found' });
        }
        return res.json(updatedVariedad);
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const success = await this.variedadService.delete(Number(id));
        if (!success) {
            return res.status(404).json({ message: 'Variedad not found' });
        }
        return res.status(204).send();
    }
}
