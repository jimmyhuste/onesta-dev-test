import { Request, Response } from 'express';
import { FrutaService } from '../../application/services/FrutaService';
import { FrutaRepository } from '../../infrastructure/repositories/frutaRepository';

const frutaRepository = new FrutaRepository();
const frutaService = new FrutaService(frutaRepository);

export class FrutaController {
    async getAll(req: Request, res: Response): Promise<Response> {
        const frutas = await frutaService.getAll();
        return res.json(frutas);
    }

    async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const fruta = await frutaService.getById(Number(id));
        if (!fruta) {
            return res.status(404).json({ message: 'Fruta not found' });
        }
        return res.json(fruta);
    }

    async create(req: Request, res: Response): Promise<Response> {
        const fruta = req.body;
        const newFruta = await frutaService.create(fruta);
        return res.status(201).json(newFruta);
    }

    async update(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const fruta = req.body;
        const updatedFruta = await frutaService.update(Number(id), fruta);
        if (!updatedFruta) {
            return res.status(404).json({ message: 'Fruta not found' });
        }
        return res.json(updatedFruta);
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const success = await frutaService.delete(Number(id));
        if (!success) {
            return res.status(404).json({ message: 'Fruta not found' });
        }
        return res.status(204).send();
    }
}
