import { Request, Response } from 'express';
import { AgricultorService } from '../../application/services/agricultorService';
import { AgricultorRepository } from '../../infrastructure/repositories/agricultorRepository';

const agricultorRepository = new AgricultorRepository();
const agricultorService = new AgricultorService(agricultorRepository);

export class AgricultorController {
    async getAll(req: Request, res: Response): Promise<Response> {
        const agricultores = await agricultorService.getAll();
        return res.json(agricultores);
    }

    async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const agricultor = await agricultorService.getById(Number(id));
        if (!agricultor) {
            return res.status(404).json({ message: 'Agricultor not found' });
        }
        return res.json(agricultor);
    }

    async create(req: Request, res: Response): Promise<Response> {
        const agricultor = req.body;
        const newAgricultor = await agricultorService.create(agricultor);
        return res.status(201).json(newAgricultor);
    }

    async update(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const agricultor = req.body;
        const updatedAgricultor = await agricultorService.update(Number(id), agricultor);
        if (!updatedAgricultor) {
            return res.status(404).json({ message: 'Agricultor not found' });
        }
        return res.json(updatedAgricultor);
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const success = await agricultorService.delete(Number(id));
        if (!success) {
            return res.status(404).json({ message: 'Agricultor not found' });
        }
        return res.status(204).send();
    }
}
