import { Request, Response } from 'express';
import { ClienteService } from '../../application/services/clienteService';
import { ClienteRepository } from '../../infrastructure/repositories/clienteRepository';

const clienteRepository = new ClienteRepository();
const clienteService = new ClienteService(clienteRepository);

export class ClienteController {
    private clienteService: ClienteService;

    constructor(clienteService: ClienteService) {
        this.clienteService = clienteService;
    }

    async getAll(req: Request, res: Response): Promise<Response> {
        const clientes = await this.clienteService.getAll();
        return res.json(clientes);
    }

    async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const cliente = await this.clienteService.getById(Number(id));
        if (!cliente) {
            return res.status(404).json({ message: 'Cliente not found' });
        }
        return res.json(cliente);
    }

    async create(req: Request, res: Response): Promise<Response> {
        const cliente = req.body;
        const newCliente = await this.clienteService.create(cliente);
        return res.status(201).json(newCliente);
    }

    async update(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const cliente = req.body;
        const updatedCliente = await this.clienteService.update(Number(id), cliente);
        if (!updatedCliente) {
            return res.status(404).json({ message: 'Cliente not found' });
        }
        return res.json(updatedCliente);
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const success = await this.clienteService.delete(Number(id));
        if (!success) {
            return res.status(404).json({ message: 'Cliente not found' });
        }
        return res.status(204).send();
    }
}
