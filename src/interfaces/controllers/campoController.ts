import { Request, Response } from 'express';
import { CampoService } from '../../application/services/campoService';
import { CampoRepository } from '../../infrastructure/repositories/campoRepository';

const campoRepository = new CampoRepository();
const campoService = new CampoService(campoRepository);

export class CampoController {
    async getAll(req: Request, res: Response): Promise<Response> {
        const campos = await campoService.getAll();
        return res.json(campos);
    }

    async getById(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const campo = await campoService.getById(Number(id));
        if (!campo) {
            return res.status(404).json({ message: 'Campo not found' });
        }
        return res.json(campo);
    }

    async create(req: Request, res: Response): Promise<Response> {
        const campo = req.body;
        const newCampo = await campoService.create(campo);
        return res.status(201).json(newCampo);
    }

    async update(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const campo = req.body;
        const updatedCampo = await campoService.update(Number(id), campo);
        if (!updatedCampo) {
            return res.status(404).json({ message: 'Campo not found' });
        }
        return res.json(updatedCampo);
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const { id } = req.params;
        const success = await campoService.delete(Number(id));
        if (!success) {
            return res.status(404).json({ message: 'Campo not found' });
        }
        return res.status(204).send();
    }
}
