import { Request, Response } from 'express';
import { CosechaService } from '../../application/services/cosechaService';

export class CosechaController {
    constructor(private cosechaService: CosechaService) {}
  
    async getAll(req: Request, res: Response): Promise<void> {
      try {
        const cosechas = await this.cosechaService.getAll();
        res.json(cosechas);
      } catch (error) {
        res.status(500).json({ error: 'Error al obtener las cosechas' });
      }
    }
  
    async getById(req: Request, res: Response): Promise<void> {
      try {
        const cosecha = await this.cosechaService.getById(Number(req.params.id));
        if (cosecha) {
          res.json(cosecha);
        } else {
          res.status(404).json({ error: 'Cosecha no encontrada' });
        }
      } catch (error) {
        res.status(500).json({ error: 'Error al obtener la cosecha' });
      }
    }
  
    async create(req: Request, res: Response): Promise<void> {
      try {
        const cosechaData = req.body;
    
        const newCosecha = await this.cosechaService.create(cosechaData);
        res.status(201).json(newCosecha);
      } catch (error) {
        console.error('Error al crear la cosecha:', error);
        res.status(500).json({ error: 'Error interno del servidor al crear la cosecha' });
      }
    }
    
  
    async update(req: Request, res: Response): Promise<void> {
      try {
        const updatedCosecha = await this.cosechaService.update(Number(req.params.id), req.body);
        if (updatedCosecha) {
          res.json(updatedCosecha);
        } else {
          res.status(404).json({ error: 'Cosecha no encontrada' });
        }
      } catch (error) {
        res.status(500).json({ error: 'Error al actualizar la cosecha' });
      }
    }
  
    async delete(req: Request, res: Response): Promise<void> {
      try {
        const success = await this.cosechaService.delete(Number(req.params.id));
        if (success) {
          res.status(204).end();
        } else {
          res.status(404).json({ error: 'Cosecha no encontrada' });
        }
      } catch (error) {
        res.status(500).json({ error: 'Error al eliminar la cosecha' });
      }
    }
}
